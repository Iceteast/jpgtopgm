#! /usr/bin/env python3
# -*- coding:utf-8 -*-

def write_pgm(img, filename = 'out.pgm'):
	
	h = len(img)
	w = len(img[0])
	print("[LOG]%d - %d" % (w, h))
	ans = "\n".join([" ".join([str(pixel) for pixel in e]) for e in degree_to_gray(img, w, h)])
	with open(filename, 'w') as f:
		f.write("P2\n")
		f.write("%d %d\n" % (w, h))
		f.write("255\n")
		f.write(ans)
	print("[LOG] fertig!")
	
def degree_to_gray(img, w, h):
	
	ans = [[0 for i in range(w)] for j in range(h)]
	for i in range(w):
		for j in range(h):
			ans[j][i] = int(img[j][i] * 255)

	return ans